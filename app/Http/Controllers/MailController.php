<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Models\Turn;
use App\Models\Feedback;
use App\Models\User;

class MailController extends Controller
{

    public function getContact(){
        
        return view('/emails/contact');
    }

    public function postContact(Request $request){
        $this->validate($request,[
            'email' => 'required|email'
        ]);
        
        $data = array(
            'name' => $request->name,
            'email' => $request->email,

            // 'feedback' => 'http://sat.com/show-feedback/'.$request->idFirebase //local
            'feedback' => 'http://165.227.183.7/show-feedback/'.$request->idFirebase //nube
        );


        Mail::send('emails.confirm',$data,function($message) use ($data){
            $message->to($data['email']);
            $message->subject('Calificacion de SAT');
            $message->from('sat.reply@gmail.com');
        });
        return $request;
    }

    public function showFeedback($idFirebase){
        $turnos = Turn::all();
        $feedbacks = Feedback::all();
        foreach ($turnos as $turno) {
            if($turno->id_firebase==$idFirebase){                    
                    foreach ($feedbacks as $feedback) {
                        if($feedback->id_turn == $turno->id){
                            return view('/emails/error');
                        }
                    }
                    return view('/emails/feedback');
            }
        }
        return view('/emails/error');
    }

    public function registerFeedback(Request $request){
        $idFirebase = $request['idFirebase'];
        $turn = Turn::where('id_firebase','=',$idFirebase)->firstOrFail();

        $feedback = new Feedback;
        $feedback->id_turn = $turn->id;
        $feedback->value = $request['estrellas'];
        $feedback->detail = $request['feedback'];
        $feedback->save();
        return $feedback;
    }

    //metodo que cambia el estado del usuario a 1(activo) cuando le da clic en el boton del correo
    public function showLoginToken(Request $request){
        $token = $request['token'];
        $usuarios = User::all();

        foreach ($usuarios as  $usuario) {
            if($usuario->remember_token == $token && $usuario->id_user_state==4){
                $usuario->id_user_state = 1;//activo
                $usuario->save();            
                // return 'Redireccionar a el login';
                header('Location: http://sat-movil.000webhostapp.com');
            }
        }
        return view('emails/error');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'hola';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
